\chapter{Conceptos básicos del movimiento del software libre}
\label{conceptos mov}

\section{¿Qué es el software libre?}

El \emph{software libre} goza de una definición precisa. Un programa es libre
si y solo si ofrece la conjunción de las siguientes 4 libertades:
\begin{enumerate}
    \item[0] La libertad de usar el programa con cualquier propósito.
    \item[1] La libertad de estudiar y modificar el programa.
    \item[2] La libertad de hacer y distribuir copias del programa original.
    \item[3] La libertad de hacer y distribuir copias de las modificaciones
             hechas al programa.
\end{enumerate}
La 0 y la 1 son las libertades individuales, mientras que la 2 y la 3 son las
libertades colectivas. Con que falte una sola de estas cuatro, ya estamos
ante un software que no es libre, lo cual se conoce como \emph{software
  privativo}, porque nos priva de libertades. Las cuatro se consideran
esenciales.

Gracias a que esta definición es bien precisa, en general resulta fácil
determinar si un programa es libre o no. Cada uno es libre de tener sus
propias concepciones sobre la libertad y nos parece muy bueno que se
reflexione sobre la misma, pero al usar el término ``software libre'', hay
una definición estándar y aceptada que se presta poco a ambigüedades, y es la
que dimos recién.

\subsection{Código fuente}

Un requisito fundamental para las libertades 1 y 3, pero que no es suficiente
para garantizarlas, es el acceso al \emph{código fuente} del programa. ¿Qué
significa esto? Lo vamos a cubrir con detalle en el capítulo \ref{fuentes},
pero veamos ahora un acercamiento...

Una computadora solamente entiende lenguaje de máquina, que consiste en
secuencias de instrucciones codificadas en binario, es decir, con ceros y
unos. Los programas no son más que secuencias tales. Pero el que hace el
programa no usa este lenguaje, porque sería demasiado engorroso; en cambio,
escribe código en lo que se llama un lenguaje de programación, que es un
lenguaje pensado para ser entendido por humanos (que tengan algunos
conocimientos). Este código es lo que se conoce como código fuente, porque es
la fuente a partir de la cual luego se genera código en el lenguaje de la
máquina.

A la hora de distribuir un programa, el desarrollador puede optar entre
distribuir el código fuente, el código máquina o ambos. A partir del primero
podemos generar el segundo, así que si distribuyéramos solamente el código
fuente, no estaríamos privando de nada al usuario. Lamentablemente, lo que se
suele hacer dentro de la lógica del software privativo es distribuir solo el
código máquina (bajo la forma de un archivo ejecutable), el cual no sirve
para regenerar el código fuente. En estos casos, el usuario puede ejecutar el
programa, pero se le complica muchísimo estudiarlo y modificarlo. Lo cual,
considerando el marco del ``copyright'', nos parece una estafa. Vemos
entonces la importancia de distribuir el código fuente si queremos hablar de
software libre.

Sin embargo, como dijimos, tener acceso a las fuentes no implica que un
programa sea libre. Esto es algo que a veces se confunde. Hay programas con
su código fuente disponible para los usuarios que aun así son privativos, ya
que no ofrecen plenamente las cuatro libertades. Por ejemplo, pueden dejarte
ver el código pero no darte permiso para modificarlo.

\section{Licencias, ``copyright'' y ``copyleft''}

En la Argentina y en muchos otros países, cuando producimos una obra
cultural, esta automáticamente pasa a estar sujeta al sistema del
``copyright''. Este sistema fue pensado en tiempos en que copiar no estaba al
alcance de cualquiera: los ciudadanos cedían a las imprentas una serie de
derechos que no podían ejercer, con el fin de darles una ventaja sobre otras
imprentas y así incentivar la producción y difusión de libros. Hoy en día la
situación es muy distinta, copiar es algo que prácticamente tiene costo
nulo. Pero en vez de adaptar las leyes a la realidad, los grandes
beneficiarios del ``copyright'' pretenden adaptar forzosamente la realidad a
las leyes. Más aun, se tiende a agregar cada vez más prohibiciones desde esas
leyes. Así, observamos que se restringe la cultura y se enriquece a unos
pocos.

El software se considera una obra cultural; eso permite que se le pueda
aplicar el ``copyright''.

Tras un período de tiempo determinado, el ``copyright'' expira y la obra pasa
al dominio público. Pero ese período tiende a infinito, porque los
legisladores de distintos países lo extienden continuamente. Para darnos una
idea, actualmente en nuestro país, en general una obra pasa al dominio
público 70 años después de la muerte del autor. Conociendo el ritmo al cual
la computación avanza y los programas se van volviendo obsoletos, ¿tiene
sentido esperar a que se muera el autor y después esperar 70 años más para
poder prestarle un programa a tu amigo de forma legal?

\marginpar{Podemos incluso ir más allá y preguntarnos: ¿tiene sentido
  criminalizar la solidaridad?, ¿tiene sentido que por ayudar al prójimo te
  comparen con un asaltador de barcos?}

% CONFIRMAR SI SE PUEDE DEJAR EN DOMINIO PÚBLICO ANTICIPADAMENTE
Como dijimos, la aplicación del ``copyright'' es automática y se asumen
``todos los derechos reservados'', lo que da al autor derechos exclusivos
para la distribución de copias, las modificaciones y el uso público de la
obra. Salvo que este explícitamente exprese lo contrario: puede indicar que
la obra está bajo dominio público, o puede, aun manteniendo el ``copyright'',
licenciarla (o sea, aplicarle una \emph{licencia}). Una licencia es un texto
con validez legal que establece los términos bajo los cuales se permite
emplear una obra, un permiso que da el autor a los usuarios de su
obra.\footnote{Si bien hablamos de autor, en realidad el beneficiario es el
  propietario de los derechos de autor. Ambos pueden coincidir, pero muchas
  veces el segundo es un empleador o una gestora colectiva de derechos de
  autor.}

Para que un programa llegue a ser libre, entonces, debe estar bajo el dominio
público o bien debe disponer de una licencia que dé las cuatro libertades de
todo programa libre –además de tener su código fuente disponible para los
usuarios–. Tales licencias se llaman ``licencias de software libre'' o
``licencias libres'', y hay muchas de ellas que se usan en multitud de
programas.

De hecho las licencias libres van más allá del software: se emplean también
para otros tipos de obras culturales, como canciones, textos (este mismo
documento está bajo una de ellas), videos, imágenes, etcétera. Algunas están
pensadas para ser usadas en un tipo de obra específico, mientras que otras
son más generales.

Podemos clasificar las licencias libres en dos grupos: las que son
``copyleft'' y las que no. ¿Qué es esto del ``copyleft''? Es una ingeniosa
idea del fundador del movimiento del software libre, Richard Stallman, que se
vale del ``copyright'' para lograr lo contrario al ``copyright'': el
``copyleft'', al aplicarse a una obra libre, establece que todo derivado de
la misma debe ser también libre. Así, por citar dos de las más populares,
decimos que la GPL de GNU es una licencia de software libre ``copyleft'' y la
BSD es no ``copyleft''. Existen muchas licencias más y ha habido muchas
discusiones entre quienes están a favor de este mecanismo y quienes no.

% ¿Vale la pena listar algunas licencias libres populares? GPL, LGPL, FDL,
% BSD, MIT, Apache, Creative Commons.

\section{GNU, Linux y GNU/Linux}

La obra insignia del movimiento del software libre es el sistema operativo
\emph{GNU/Linux}. Este está compuesto de dos partes: \emph{GNU}, que es todo
el sistema operativo menos el núcleo, y \emph{Linux}, que es el núcleo.

Un \emph{sistema operativo} es un conjunto de software que convierte a una
computadora en algo útil para el usuario. Todo sistema operativo consta de un
\emph{núcleo}, que es el programa principal o el corazón del sistema; este
gestiona los programas que ejecuta el usuario, gestiona el hardware y sirve de
intermediario entre ambos.

Más adelante vamos a ver todos los detalles técnicos con mucha más
profundidad. Ahora solo damos una introducción para que se entienda el
capítulo que sigue.

\section{Más información}

\begin{enumerate}
    \item (definición de software libre en gnu.org)
    \item (lista de licencias aprobadas por la FSF)
    \item (lista de licencias aprobadas por la OSI)
\end{enumerate}
