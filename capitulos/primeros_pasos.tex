\chapter{Primeros pasos con la línea de comandos}
\label{primeros pasos}

\section{Comandos, archivos y directorios}

\subsection{Debutando con la línea de comandos}

Para empezar a introducirnos en GNU/Linux, vamos a aprender sobre la línea de
comandos y el manejo de archivos.

Primero veamos un poco cómo es el panorama. Como cualquier sistema operativo
de propósito general, GNU/Linux nos permite ejecutar programas. ¿Cómo ejecuta
un usuario estos programas? La forma más elemental es mediante líneas de
comando: escribimos una línea de texto que indica qué programa/comando
ejecutar y qué datos pasarle, apretamos Enter y el sistema pasa a interpretar
la línea.

El formato de una línea de comando es el siguiente:
\begin{verbatim}
    nombrecomando argumento1 argumento2 ...
\end{verbatim}
Lo primero que se escribe es el nombre del comando o programa a ejecutar (más
adelante veremos cómo sabe el sistema con qué programa se corresponde cada
nombre). Luego vienen los argumentos: texto que el programa recibe de parte
del usuario, por ejemplo nombres de archivos sobre los cuales operar u
opciones para cambiar el comportamiento. La cantidad y el formato de los
argumentos depende del comando a ejecutar: puede que no haga falta ninguno,
que haya que poner algunos de forma obligatoria, y otros por su parte pueden
ser opcionales. Entre dos argumentos, o entre el nombre del comando y el
primer argumento, deben ir uno o más espacios; por otro lado, al principio y
al final de la línea de comando no hacen falta espacios, pero es inofensivo
ponerlos ya que se ignoran.

Es importante tener en cuenta que acá se distinguen mayúsculas de minúsculas:
es distinto un comando \texttt{hola} de un comando \texttt{HOLA}. La gran
mayoría de los nombres de comando van completamente en minúscula, pero esto
es por convención, no hay nada que obligue a que sea así.

Para no aburrirnos mucho, pasemos un rato a la acción. Supongamos que estás
usando GNU/Linux con alguna interfaz gráfica. Entonces tendrás algún programa
emulador de terminal, con un nombre parecido a “terminal” o “consola”;
algunos ejemplos de tales programas son \emph{GNOME Terminal}, \emph{Konsole}
y \emph{xterm}. Estos te permiten ejecutar comandos desde la interfaz
gráfica. Abrí el que tengas en tu sistema y escribí esta línea de comando:
\begin{verbatim}
    ls
\end{verbatim}
O sea, nada más que la letra ele minúscula seguida de la letra ese minúscula.
Por último apretá Enter.

¡Felicitaciones! Ejecutaste tu primera línea de comando.

En particular ejecutaste el comando \texttt{ls} sin argumentos. ¿Qué hace
este comando? Muestra un listado (“ls” es una abreviatura de “listar”) de los
archivos del directorio actual.

\subsection{Archivos y directorios}

Antes de seguir ejecutando comandos, nos va a venir bien entender unas
cuantas cosas más. Un concepto fundamental en GNU/Linux es el de
\emph{archivo}, también llamado \emph{fichero}. Es un concepto bastante
conocido: consiste en una serie de datos (bytes) guardados en algún tipo de
memoria y que tienen asignado un nombre para poder acceder a ellos. Esto se
aplica a GNU/Linux así como a muchos otros sistemas, pero en el primero cabe
destacar ciertas particularidades:
\begin{enumerate}
    \item Por un lado, muchas cosas que en otros sistemas no son archivos acá
          sí lo son. Existen de 7 tipos distintos; entre ellos, los que uno
          llama comúnmente archivos son los llamados archivos regulares.
    \item Por otro lado, existe una separación entre el contenido de un
          archivo y el nombre, y gracias a esto, un archivo puede tener más
          de un nombre.
    \item Al igual que en los nombres de comando, en los nombres de archivo
          se distinguen mayúsculas de minúsculas.
\end{enumerate}

Otro tipo de archivo, importantísimo, son los directorios. Un
\emph{directorio}, también llamado \emph{carpeta}, es un contenedor de
archivos; en el caso de GNU/Linux, un archivo que sirve para contener otros
archivos. Un concepto bastante extendido también. Pero lo más interesante
está por venir.

Antes habíamos hablado del directorio actual. ¿Qué significa esto? Cuando
estamos en una terminal (y de hecho, cuando usamos cualquier programa, pero
acá es donde se observa más claramente), tenemos una variable llamada
\emph{directorio actual} o \emph{directorio de trabajo}. En otras palabras,
en cada momento hay asociado un directorio a nuestra sesión de trabajo, con
el objetivo de facilitarnos operar sobre los archivos contenidos en el
mismo. Al abrir una terminal, suele asignarse como actual el directorio
personal del usuario. Por lo tanto, lo que mostró antes \texttt{ls}
probablemente haya sido el contenido del directorio personal.

Hay dos comandos para lidiar directamente con el directorio actual:
\texttt{pwd} y \texttt{cd}. El primero es muy simple, podés probarlo
escribiendo esta línea:
\begin{verbatim}
    pwd
\end{verbatim}
\emph{pwd} son las siglas de “print working directory”, es decir, “imprimir
directorio de trabajo”. Imprimir en este caso no significa plasmar en papel
por medio de una impresora, sino algo más general: enviar un texto como
salida a algún lado, que en nuestro caso va a ser la pantalla; este es un
significado muy común del término. El comando no hace más que mostrar la ruta
del directorio actual.

\subsection{Rutas}

Antes de ver el segundo comando, entendamos mejor esto de las rutas. Dijimos
que un directorio era un contenedor de archivos, y también dijimos que un
directorio era un archivo. De estas dos premisas derivamos que un directorio
puede contener otros directorios. Además, cabe hacer otra observación: si nos
restringimos a un único nombre por archivo, cada archivo está contenido en
exactamente un directorio. Estas dos características nos permiten armar
\emph{árboles/jerarquías de directorios}. Si A y B son directorios y A
contiene a B, decimos que A es padre de B, que B es hijo de A o que B es
subdirectorio de A. Como en todo árbol (matemático), hay un elemento que es
la raíz, y se llama, cómo no, \emph{directorio raíz} (en inglés, “root”).

GNU/Linux dispone de una jerarquía de directorios determinada. La raíz
contiene directorios del sistema, cada uno con un propósito específico. En el
capítulo \ref{discos} veremos esto en detalle. Ahora solo nos interesa un
directorio en particular, el indicado con la ruta \texttt{/home/usuario}
(reemplazando \texttt{usuario} por el nombre de usuario en el sistema).

Una \emph{ruta} es simplemente la dirección para encontrar un archivo a lo
largo de la jerarquía. Hay dos tipos de rutas: \emph{absolutas} y
\emph{relativas}.
\begin{itemize}
    \item Una ruta absoluta indica el camino desde el directorio raíz hasta
          el archivo deseado. Empieza con una barra (\texttt{/}), que
          representa la raíz, y después incluye los nombres, en orden, de
          cada nodo del camino, separados por más barras. Salvo quizá el
          último, todos los nodos deben ser directorios. Por ejemplo,
          \texttt{/home/usuario} representa el directorio al que llegamos
          partiendo desde la raíz, entrando al subdirectorio \texttt{home} y
          dentro de este, a \texttt{usuario}; cabe destacar que nosotros
          sabemos que \texttt{usuario} es un directorio, pero no hay nada en
          la ruta que nos lo indique, por lo que podría ser cualquier tipo de
          archivo. Hay dos nombres de directorio especiales: \texttt{.} y
          \texttt{..}. El primero denota el directorio actual y el segundo
          denota el padre del actual. Así, son equivalentes
          \texttt{/home/usuario}, \texttt{/home/./usuario} y
          \texttt{/home/../home/usuario}.
    \item Una ruta relativa, en vez de partir de la raíz, parte del
          directorio actual. Se identifica por la ausencia de barra al
          principio. Así, si estamos en \texttt{/home}, \texttt{usuario}
          serviría como ruta relativa con el mismo destino que
          \texttt{/home/usuario}; y si estamos en \texttt{/}, lo haría
          \texttt{home/usuario}. Además, considerando que podemos notar el
          directorio actual explícitamente con \texttt{.}, es común agregar
          \texttt{./} al principio de una ruta relativa; por ejemplo,
          \texttt{./usuario}. Hay casos en que esto último es necesario, para
          saber que estamos hablando de una ruta y no de otra cosa.
\end{itemize}
Un par de pequeñas observaciones sobre el uso de barras. En todo lugar donde
vaya una barra, se puede poner más de una. Y al final de una ruta, en caso de
que el archivo apuntado sea un directorio, se pueden poner cero o más.

Ya estamos en condiciones de ver el comando que nos quedaba, \texttt{cd}. Su
nombre viene de “cambiar directorio”. Y, sorprendentemente, sirve para
cambiar el directorio actual, es decir, para ir a otro directorio. Se puede
ejecutar de tres maneras:
\begin{enumerate}
    \item Sin argumentos:
          \begin{verbatim}
    cd
          \end{verbatim}
          Establece como actual el directorio personal. Es lo mismo que:
          \begin{verbatim}
              cd /home/usuario
          \end{verbatim}
    \item Con una ruta como argumento:
          \begin{verbatim}
    cd ruta
          \end{verbatim}
          Establece como actual el directorio cuya ruta se haya pasado como
          argumento.
    \item Con un guion medio (\texttt{-}) como argumento:
          \begin{verbatim}
    cd -
          \end{verbatim}
          Establece como actual el directorio que era actual previamente.
          Esto es muy útil para ir alternando entre dos directorios, ya que
          nos permite volver un paso atrás.
\end{enumerate}

Sabemos ya lo suficiente para navegar por la jerarquía de directorios. Con
\texttt{ls} y \texttt{cd}, podemos ir de un lado a otro y ver qué archivos
hay en cada rincón. Estos son dos de los comandos más importantes en
GNU/Linux, se usan a cada rato, así que conviene conocerlos bien.
\texttt{pwd}, en cambio, es muy raro que se use de forma directa, por lo
que vamos a ver ahora.

Al escribir líneas de comando, probablemente hayas observado que en la
terminal queda escrito, generalmente a la izquierda de donde se escriben
aquellas, un pequeño texto que brinda diversa información. Probablemente
termine con un signo pesos (\texttt{\$}) y un espacio. Este texto se llama
“prompt” y cumple dos funciones: la primera es indicar al usuario que el
sistema está listo para recibir texto ingresado por aquel; la segunda es
darle información al usuario sobre la sesión.

Entre los datos que muestra el “prompt”, suele estar la ruta del directorio
actual (con el detalle de que \texttt{/home/usuario} se representa con
\texttt{\~}). Es por esto que \texttt{pwd} no se usa directamente: la
información que brinda ya nos aparece automáticamente antes de cada línea
que escribimos.

Si bien el “prompt” no es parte de la línea de comando en sí, es común usarlo
para identificar las mismas. En diversa documentación sobre GNU/Linux, nos
vamos a encontrar líneas de comando descritas como la siguiente:
\begin{verbatim}
    $ ls
\end{verbatim}
Cuando veamos algo así, tenemos que acordarnos de que el signo pesos es parte
del “prompt” y que no hay que escribirlo.

\section{Más herramientas de navegación}

Sigamos aprendiendo comandos para navegar por archivos y directorios.

Sabemos cómo ver lo que hay en un directorio, pero ¿cómo vemos el contenido
de un archivo regular? Si el archivo contiene texto, usamos el comando
\texttt{cat}. Este es un comando multipropósito, no porque sus programadores
se hayan encargado de incluirle muchas funcionalidades –de hecho tiene muy
pocas–, sino porque está diseñado para aprovechar facilidades externas que
nos brinda GNU/Linux y que vamos a ver más adelante. El nombre del comando
viene de “concatenar”, ya que uno de sus usos es concatenar archivos, es
decir, juntar el contenido de varios archivos, uno tras otro, en uno
solo. Nosotros ahora lo vamos a usar para imprimir el contenido de archivos
regulares.

A \texttt{cat} se le pasan como argumento las rutas de los archivos a
imprimir:
\begin{verbatim}
    cat archivo1 archivo2 ...
\end{verbatim}
Hagamos una prueba. Supongamos que queremos ver la lista de todos los
usuarios que tiene el sistema operativo. Esta información se guarda en el
archivo \texttt{/etc/passwd}, así que podemos hacer:
\begin{verbatim}
    cat /etc/passwd
\end{verbatim}
y disfrutar del espectáculo.

\section{Seamos más que espectadores}

Ya tenemos cubierta la parte de navegación básica, sabemos movernos y revisar
los archivos existentes. Pero quedarnos como meros espectadores o
consumidores no es muy divertido cuando podemos además involucrarnos para
cambiar las cosas. Veamos entonces cómo crear, modificar y borrar archivos.

Para crear archivos hay un comando por cada tipo de archivo. Vamos a ver los
dos que corresponden a los tipos que vimos. El primero es \texttt{touch}, que
crea archivos regulares. El nombre viene de la palabra inglesa para “tocar”,
porque si se aplica a un archivo existente, da la idea de que lo toca y
entonces se actualizan sus fechas de acceso y modificación. Y el segundo
comando es \texttt{mkdir} –abreviatura de “make directory”, “hacer
directorio”–, que como habrás adivinado, crea directorios.

Con el comando \texttt{cp}, de “copiar”, copiamos archivos. Hay dos formas de
invocarlo:
\begin{verbatim}
    cp origen destino
    cp archivo1 archivo2 archivo3 ... directorio
\end{verbatim}
En la primera forma, \texttt{destino} debe ser una ruta inexistente; en este
caso se crea el archivo con ruta \texttt{destino} y con el mismo contenido
que el archivo con ruta \texttt{origen}. En la segunda forma se pasan rutas a
uno o más archivos existentes primero, y luego la ruta a un directorio
también existente; lo que se hace es generar copias de los primeros archivos
en el directorio, con los mismos nombres.

Veamos un ejemplo de todo esto:
\begin{verbatim}
    touch a b c lorem
    mkdir d
    cp lorem ipsum
    cp a b c d
\end{verbatim}
tras ejecutar estas cuatro líneas de comando, queda la siguiente jerarquía:
\begin{verbatim}
    .
    |- a
    |- b
    |- c
    |- d
    |  |- a
    |  |- b
    |  '- c
    |- ipsum
    '- lorem
\end{verbatim}
cosa que podemos comprobar usando \texttt{ls} y \texttt{cd}.

Otro ejemplo:
\begin{verbatim}
    mkdir a b
    cp a b
\end{verbatim}
Copiamos un directorio \texttt{a} en un directorio \texttt{b}. ¿Qué pasó acá?
El comando da error, porque por defecto no copia directorios enteros. Para
que lo haga tenemos que pasarle una opción que cambia su comportamiento: la
opción \texttt{-r}, que lo hace funcionar de manera recursiva:
\begin{verbatim}
    cp -r a b
\end{verbatim}
Ahora se genera \texttt{b/a}, como queríamos.

Las opciones de línea de comando vienen en dos formatos: cortas y largas. La
que acabamos de usar es corta, lleva un solo guion al principio y
seguidamente tiene una sola letra. Las largas llevan dos guiones y
suficientes letras para formar una o más palabras enteras. La mayoría vienen
de a pares, es decir, hay tanto una opción corta como una larga que cumplen
la misma función. En el caso de \texttt{-r}, la alternativa larga es
\texttt{--recursive}. Habría sido lo mismo hacer, en el ejemplo anterior:
\begin{verbatim}
    cp --recursive a b
\end{verbatim}
Las opciones largas son más descriptivas, pero las cortas son más fáciles de
escribir e incluso se pueden combinar cuando hay más de una, gracias al hecho
de que cada una lleva una sola letra. Por ejemplo, supongamos que tenemos un
comando \texttt{hola} con opciones \texttt{-a}, \texttt{-b} y \texttt{-c};
sería lo mismo hacer:
\begin{verbatim}
    hola -a -b -c
\end{verbatim}
que hacer:
\begin{verbatim}
    hola -abc
\end{verbatim}

Hay casos en que existe una opción en un formato pero no una equivalente en
el otro. Incluso hay comandos que no tienen ninguna opción larga. Esto puede
deberse a que el formato largo es una extensión de GNU y entonces puede haber
comandos surgidos en otros sistemas operativos de tipo Unix, que no las
tienen. O puede haber programadores que no quieran o no se hayan molestado en
agregarlas a sus programas. También hay unos pocos comandos que no siguen
estas convenciones de opciones largas y cortas sino que tienen otros
formatos, pero son casos muy particulares.

Dependiendo del comando, puede ser lo mismo escribir las opciones en
cualquier orden y posición entre los argumentos o no. Para la mayoría de los
comandos, incluidos todos los que vemos en este capítulo, es lo mismo.

De forma similar a \texttt{cp}, está el comando \texttt{mv}, de “mover”, para
mover archivos o cambiarles el nombre. El formato de los argumentos es
similar, con la diferencia de que no hace falta la opción \texttt{-r} para
mover directorios. ¿Por qué no hace falta? A lo mejor sea porque la forma de
programar estos dos comandos es distinta: para copiar, uno tiene que crear un
archivo nuevo y probablemente duplicar todo el contenido; en cambio, para
mover, solo hay que alterar el nombre del archivo o su ubicación. Nada impide
a los programadores ocultar estos detalles bajo una interfaz de usuario
común, pero se ve que no fue lo que decidieron hacer en este caso.

Para borrar tenemos dos comandos: \texttt{rm} y \texttt{rmdir}. Los nombres
vienen de “remove” y “remove directory”, que significan “borrar” y “borrar
directorio”. En el caso de archivos regulares usamos \texttt{rm}. Para
directorios te imaginarás que usamos \texttt{rmdir}, y en algunos casos sí,
pero es bastante limitado: solo sirve para directorios vacíos. Así que cuando
tenemos directorios con cosas adentro, debemos usar \texttt{rm} con la opción
\texttt{-r}. La limitación de \texttt{rmdir} sin embargo puede ser útil para
precaución: si estamos por borrar un directorio que creíamos vacío y resulta
que en realidad no lo está, esto nos puede salvar de borrar accidentalmente
cosas que no queramos perder. Lo que hacen estos dos comandos es borrar el
nombre de archivo pasado como argumento: el contenido puede seguir estando,
pero ya deja de ser accesible por medio de ese nombre; una vez borrados todos
los nombres, se pierde toda forma de acceder al contenido\footnote{Haciendo
  análisis de bajo nivel se puede llegar a recuperar el contenido, pero es
  algo relativamente complicado. Además está el riesgo de que el espacio
  donde se encontraba el contenido se sobrescriba con contenido de otros
  archivos.}. Estos comandos no usan la papelera de reciclaje, así que hay
que tener cuidado.

\section{Afilemos las herramientas de navegación}

Volvamos un poco a la parte de navegación. Vamos a ver cómo consultar más
información sobre los archivos.

Hasta ahora, con \texttt{ls}, solo veíamos los nombres. Pero existe una
opción que le podemos pasar al invocarlo, la opción \texttt{-l}, que hace que
imprima mucha más información. Disfrutala:
\begin{verbatim}
    ls -l
\end{verbatim}
La salida puede ser algo así:
\begin{verbatim}
    total 12K
    -rw-r--r-- 1 mariano users   12 abr  7 23:12 a
    -rw-r--r-- 1 mariano users   43 abr  7 23:12 b
    -rw-r--r-- 1 mariano users    0 abr  7 23:11 c
    drwxr-xr-x 2 mariano users 4,0K abr  7 23:12 d/
\end{verbatim}
La primera línea indica el tamaño total ocupado por todos los archivos del
directorio. Después hay una línea por cada archivo, con las siguientes
columnas:
\begin{enumerate}
    \item Tipo (el primer caracter: \texttt{-} para archivos regulares,
          \texttt{d} para directorios) y permisos.
    \item Cantidad de nombres.
    \item Usuario propietario.
    \item Grupo propietario.
    \item Tamaño.
    \item Fecha y hora de modificación.
    \item Nombre.
\end{enumerate}

\texttt{ls} tiene muchísimas opciones más, con las cuales se puede cambiar
la información que muestra, el orden de los archivos, indicar qué archivos
mostrar (o sea, filtrar), etc.

\section{Pedile ayuda a la línea de comandos}

Para concluir el capítulo, vamos a hablar de algo muy importante: los
mecanismos de que disponemos en la línea de comandos para obtener ayuda sobre
la misma.

Por empezar, hay una característica llamada autocompletado: cuando estamos
escribiendo una línea, podemos apretar la tecla Tab para que el sistema
complete partes de la misma por nosotros. Por ejemplo, si queremos ejecutar
el comando \texttt{holamundo}, podemos escribir \texttt{hol}, apretar Tab y
dejar que el resto se escriba solo. Si hay más de un comando cuyo nombre
empiece con \texttt{hol}, dependiendo de la configuración del intérprete de
comandos el compartamiento será uno u otro, pero en general con apretar Tab
una o dos veces se muestran todas las ocurrencias encontradas. El
autocompletado funciona para nombres de comando y para argumentos también. Es
algo extremadamente útil para usar la línea de comandos de forma eficiente y
cómoda.

Pero yendo al tema de la sección, el autocompletado puede servir además para
guiarnos. Si sabemos que el nombre de un comando empieza con \texttt{h} pero
no lo recordamos entero, podemos poner \texttt{h}, apretar Tab y ver los
resultados. Con la correcta configuración, podemos darnos una idea del tipo
de argumentos que recibe un comando e incluso obtener descripciones de sus
opciones.

La mayoría de los comandos ofrecen un par estandarizado de opciones:
\texttt{-h} y \texttt{--help} (“help” es “ayuda” en inglés). Al ejecutarlos
con alguna de estas, los comandos no hacen lo que normalmente harían sino que
en cambio muestran una descripción de cómo se los debe invocar: qué opciones
y otros argumentos de línea de comando admiten. Por ejemplo, se puede hacer:
\begin{verbatim}
    cp -h
\end{verbatim}
\texttt{ls} en particular interpreta \texttt{-h} de forma distinta, así que
hay que usar la opción larga:
\begin{verbatim}
    ls --help
\end{verbatim}

Por último, hay dos comandos muy útiles, que nos ofrecen más ayuda
todavía. El primero es \texttt{man}, cuyo nombre viene de “manual”. Se le
pasa como argumento el nombre de un comando y muestra una página de manual
sobre el mismo. La página contiene: una explicación de la función que cumple
el comando; descripciones de cómo ejecutar el comando y de cada opción
disponible; información sobre los autores, cómo contactarlos y cómo reportar
errores. Prácticamente todo comando cuenta con su página de manual, y es uno
de los primeros recursos a los que se acude cuando se quiere saber más sobre
un comando. También hay páginas de manual para otras cosas aparte de
comandos.

Al ejecutarse, \texttt{man} muestra la salida por medio de un programa
llamado paginador. Este se encarga de presentar solo la cantidad de texto que
quepa en pantalla y permite al usuario desplazarse para ver el resto del
texto, como si avanzara o retrocediera por páginas. El paginador más común
hoy en día es \texttt{less}, que permite desplazarse en ambas direcciones y
ofrece diversas mejoras sobre un paginador más antiguo, llamado \texttt{more}
(los nombre significan respectivamente “menos” y “más”; para completar, más
tarde salió uno llamado \texttt{most}: “lo más”, pero este no es tan usado).
Para desplazarse hacia abajo están las teclas Espacio, Abajo y AvPág; para
desplazarse hacia arriba, las teclas Arriba y RePág; para terminar el
programa, la tecla \texttt{q}.

El segundo comando es \texttt{info}. Este va más allá de \texttt{man}: en vez
de mostrar páginas de manual, ofrece manuales completos. Es parte de un
sistema de documentación desarrollado por GNU para hacer manuales tanto
digitales como impresos, y ofrece más poder que el sistema de documentación
que hay atrás de \texttt{man}. Lamentablemente no está demasiado extendido:
la mayoría de los comandos no cuentan con un manual de \texttt{info}. Pero
casi todos los manuales que hay disponibles son muy recomendables.

\texttt{info} no usa paginador externo, sino que se encarga él mismo de
mostrar la salida y permitir navegar por la misma. Para aprender a manejarlo
bien lo más recomendable es leer su propio manual con:
\begin{verbatim}
    info info
\end{verbatim}
Como referencias rápidas para sobrevivir, con las teclas Arriba, Abajo,
Izquierda y Derecha uno puede mover el cursor dentro de \texttt{info}; una
vez situado sobre un pedazo de texto que lleva a alguna otra parte del
manual, se puede apretar Enter para viajar; y para terminar el programa
tenemos la tecla \texttt{q}.

Muchos de los comandos que vimos en este capítulo forman parte de un paquete
llamado \texttt{coreutils}, y su documentación en \texttt{info} se encuentra
en el manual de dicho paquete. Si ejecutás:
\begin{verbatim}
    info coreutils
\end{verbatim}
te vas a encontrar con abundante documentación no solo de comandos que vimos
sino de muchísimos otros.

Los manuales de Info escritos por el proyecto GNU también pueden consultarse
en Internet: \textsf{http://www.gnu.org/manual/manual}. Además la FSF vende
algunos en papel, así que si tenés ganas de colaborar financieramente con la
Fundación o simplemente de tener el libro en formato de árbol muerto con una
hermosa tapa, esta es una manera: \texttt{http://shop.fsf.org/category/books/}.

\section{Resumen}

Para resumir, en este capítulo aprendimos el uso básico de la línea de
comandos de GNU/Linux, exploramos los conceptos de archivo, directorio,
jerarquía de directorios y rutas y usamos los siguientes comandos:
\begin{itemize}
    \item \texttt{ls}: lista los archivos de un directorio
    \item \texttt{pwd}: muestra la ruta del directorio actual
    \item \texttt{cd}: cambia de directorio actual
    \item \texttt{cat}: muestra el contenido de archivos regulares
    \item \texttt{touch}: crea archivos regulares o modifica sus fechas
    \item \texttt{mkdir}: crea directorios
    \item \texttt{cp}: copia archivos
    \item \texttt{mv}: mueve archivos
    \item \texttt{rm}: borra archivos
    \item \texttt{rmdir}: borra directorios vacíos
    \item \texttt{man}: muestra una página de manual
    \item \texttt{info}: muestra un manual de Info
\end{itemize}

\section{Más información}

\begin{itemize}
    \item Páginas de manual: \texttt{man ls}, \texttt{man pwd},\texttt{man
          cat}, \texttt{man touch}, \texttt{man mkdir}, \texttt{man cp},
          \texttt{man mv}, \texttt{man rm}, \texttt{man rmdir}, \texttt{man
          man}, \texttt{man info}, \texttt{man 7 units}
    \item Manuales de Info: \texttt{info coreutils}, \texttt{info info}
\end{itemize}

\section{Ejercicios}

\begin{enumerate}
    \item Supongamos que estás en una terminal con \texttt{/home/usuario}
          como directorio actual. ¿Cuál es la línea de comando más corta que
          podés escribir para ir al directorio...
          \begin{enumerate}
              \item ...raíz?
              \item ...\texttt{/home}?
              \item ...\texttt{/home/usuario}?
              \item ...\texttt{/home/usuario/.config}?
              \item ...\texttt{/home/usuario/.config/gtk-3.0}?
              \item ...\texttt{/home/otrousuario}?
              \item ...\texttt{/bin}?
          \end{enumerate}
    \item ¿En qué directorios terminamos tras ejecutar las siguientes
          secuencias de líneas de comando? Calculá las rutas absolutas sin
          tocar ni mirar la terminal.
          \begin{enumerate}
              \item
                  \begin{verbatim}
    cd /
    cd usr
    cd ./local
    cd ..
    cd share/
                  \end{verbatim}
              \item
                  \begin{verbatim}
    pwd
    cd /etc
    ls
    cd .
    cd X11/xorg.conf.d
                  \end{verbatim}
              \item
                  \begin{verbatim}
    cd ..
    cd -
    cd
    cd .
    cd //
    cd ..
                  \end{verbatim}
          \end{enumerate}
    \item Vamos a definir la equivalencia entre dos rutas como la propiedad
          de que ambas lleven al mismo archivo, suponiendo que existan todos
          los nodos nombrados en las mismas. Para cada ruta absoluta obtenida
          en el ejercicio anterior, pensá dos rutas absolutas distintas pero
          equivalentes.
    \item Encontrá, en los siguientes conjuntos de rutas, la que no sea
          equivalente a las demás:
          \begin{enumerate}
              \item \begin{itemize}
                        \item \texttt{./hola/../mundo}
                        \item \texttt{./mundo}
                        \item \texttt{./Mundo}
                        \item \texttt{mundo}
                        \item \texttt{hola/Mundo/../../mundo}
                    \end{itemize}
              \item \begin{itemize}
                        \texttt{/usr/lib}
                        \texttt{/usr/lib/}
                        \texttt{/usr//lib}
                        \texttt{/./usr/lib}
                        \texttt{/../usr/lib}
                        \texttt{/usr/../lib}
                    \end{itemize}
          \end{enumerate}
    \item ¿Cuál sería un comando o una serie de comandos para...
          \begin{itemize}
              \item ...ver el contenido de los archivos \texttt{uno},
                    \texttt{dos}, \texttt{tres} y \texttt{cuatro} en
                    orden inverso al que se usa acá para mencionarlos
                    (es decir, que primero se imprima \texttt{cuatro},
                    después \texttt{tres}, etcétera)?
              \item ...mover todos los archivos PDF del directorio
                    personal a un subdirectorio \texttt{documentos}?
              \item ...borrar todo lo que haya en el escritorio?
              \item ...hacer una copia de seguridad de todas las cosas
                    personales de un usuario?
              \item ...juntar los archivos contenidos en 3 directorios,
                    \texttt{a}, \texttt{b} y \texttt{c}, en un solo
                    directorio, suponiendo que la jerarquía sea la
                    siguiente?:
                    \begin{verbatim}
    .
    |- a
    |  |- a_hola
    |  '- a_mundo
    |- b
    |  |- b_hola
    |  '- b_mundo
    '- c
       |- c_hola
       '- c_mundo
                    \end{verbatim}
              \item ...hacer lo del punto anterior pero con esta otra
                    jerarquía?:
                    \begin{verbatim}
    .
    |- a
    |  |- hola
    |  '- mundo
    |- b
    |  |- hola
    |  '- mundo
    '- c
       |- hola
       '- mundo
                    \end{verbatim}
          \end{itemize}
    \item En base a todo lo aprendido, investigá sobre el comando
          \texttt{stat}. ¿Para qué sirve? ¿Qué argumentos hay que pasarle?
          ¿Admite opciones que cambien su comportamiento?
    \item Hacé lo mismo de antes con el comando \texttt{clear}.
    \item Pensá alguna secuencia de operaciones que hagas comúnmente con un
          administrador de archivos gráfico. ¿Podés realizarla con los
          comandos que vimos?
\end{enumerate}
